const express = require('express')
const fs = require('fs')
const app = express()
const fetch = require('node-fetch')
const process = require('process')
require("dotenv").config()

const DEFAULT_PORT = 80

// Camera IP: 172.31.200.3

const { CAMERA_IP, CAMERA_USERNAME, CAMERA_PASSWORD, NODE_PORT } = process.env

const PORT = NODE_PORT || DEFAULT_PORT

const sendCameraCommand = async (cmd, stopCmd) => {
    fetch(`http://${CAMERA_IP}/cgi-bin/CGIProxy.fcgi?cmd=${cmd}&usr=${CAMERA_USERNAME}&pwd=${CAMERA_PASSWORD}`)
    
    if (stopCmd) {
        setTimeout(() => {
            fetch(`http://${CAMERA_IP}/cgi-bin/CGIProxy.fcgi?cmd=${stopCmd}&usr=${CAMERA_USERNAME}&pwd=${CAMERA_PASSWORD}`)
        }, 250)
    }
}

const FETCH_INTERVAL = 100
let img = fs.readFileSync("480-katt.jpg")
setInterval(async () => {
    try {
        let imgres = await fetch(`http://${CAMERA_IP}/cgi-bin/CGIProxy.fcgi?cmd=snapPicture2&usr=${CAMERA_USERNAME}&pwd=${CAMERA_PASSWORD}`)
        img = await imgres.buffer()    
    } catch (e) {
        console.log(new Date(), "unable to fetch camera image", e.message);
    }
    
}, FETCH_INTERVAL)


app.get('/axis-cgi/jpg/image.cgi', async (req, res) => {
    res.setHeader("Content-Type", "image/jpeg")
    res.send(img)
    return
})

app.get('/axis-cgi/com/ptz.cgi', async (req, res) => {
    console.log(new Date(), req.method, req.path, req.query);

    const {rpan, rtilt, rzoom, move} = req.query

    if (rpan === "3") {
        sendCameraCommand("ptzMoveRight", "ptzStopRun")
    } else if (rpan === "-3") {
        sendCameraCommand("ptzMoveLeft", "ptzStopRun")
    } else if (rtilt === "3") {
        sendCameraCommand("ptzMoveUp", "ptzStopRun")
    } else if (rtilt === "-3") {
        sendCameraCommand("ptzMoveDown", "ptzStopRun")
    } else if (rzoom === "100") {
        sendCameraCommand("zoomIn", "zoomStop")
    } else if (rzoom === "-100") {
        sendCameraCommand("zoomOut", "zoomStop")
    } else if (move === "home") {
        sendCameraCommand("ptzReset")
    }

})

app.get(/.*/, (req, res) => {
    console.log(new Date(), req.method, req.path, req.query);

    res.send("")
})

app.listen(PORT, () => {
    console.log(new Date(), "Camera Proxy started for camera at", CAMERA_IP)
})

process.on('SIGINT', () => {
  console.info(new Date(), "SIGINT received, stopping")
  process.exit(0)
})
process.on('SIGTERM', () => {
    console.info(new Date(), "SIGTERM received, stopping")
    process.exit(0)
  })
