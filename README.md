# Foscam Furuno Proxy
A simple proxy that proxies requests between Furuno plotters and Foscam cameras

## Installation
After cloning the repository, install the node dependencies and create the config file:
```bash
yarn install
cp .env.example .env
```

After creating the config file, fill in your camera IP, username and password.

An example systemd unit-file is included. Copy this to `/etc/systemd/system/FurunoFoscamProxy.service` and update the path to `app.js` in the `ExecStart` directive.

To enable and start the service run:
```bash
sudo systemctl enable FurunoFoscamProxy
sudo systemctl start FurunoFoscamProxy
```

> Note, the application binds on Port 80 by default, this is configurable through the config variable `NODE_PORT`

To update the running config you must restart the service:
```bash
sudo systemctl restart FurunoFoscamProxy
```

## Docker
The included Dockerfile contains everything to get the environment running.
Either create the config file as described above, then build the image using `docker build` to bake the config into the image, or specify the config variables as environment variables when running the container:
```bash
docker build -t foscamfurunoproxy
docker run -e CAMERA_IP=127.0.0.1 -e CAMERA_USERNAME=username -e CAMERA_PASSWORD=password foscamfurunoproxy
```

### Docker compose example
```yaml
version: "2"
services:
  proxy:
    build: .
    ports:
    - "80:80"
    environment:
      CAMERA_IP: 127.0.0.1
      CAMERA_USERNAME: username
      CAMERA_PASSWORD: password
```

## Implemented features
- [x] Image
- [x] PTZ move up/down/left/right
- [x] PTZ return to home
- [ ] Video streaming?
