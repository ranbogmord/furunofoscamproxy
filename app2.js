const {readFileSync} = require('fs');
const {createServer} = require('http');
const {EventEmitter} = require('events');

let bufferIndex = -1;
const buffers = [
	readFileSync('image.jpg'),
    readFileSync('image2.jpg'),
    readFileSync('image3.jpg'),
    readFileSync('image4.jpg'),
];


const emitter = new EventEmitter();
const server = createServer((req, res) => {
	res.writeHead(200, {
		'Cache-Control': 'no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0',
		Pragma: 'no-cache',
		Connection: 'close',
		'Content-Type': 'multipart/x-mixed-replace; boundary=--myboundary'
	});

	const writeFrame = () => {
		const buffer = buffers[bufferIndex];
		res.write(`--myboundary\nContent-Type: image/jpg\nContent-length: ${buffer.length}\n\n`);
		res.write(buffer);
	};

	writeFrame();
	emitter.addListener('frame', writeFrame);
	res.addListener('close', () => {
		emitter.removeListener('frame', writeFrame);
	});
});
server.listen(80);


setInterval(() => {
	bufferIndex++;
	if (bufferIndex >= buffers.length){
		bufferIndex = 0;
	}
	emitter.emit('frame');
}, 1000);
