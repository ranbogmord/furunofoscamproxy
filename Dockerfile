FROM node:17

RUN apt-get update && apt-get install -y libgd-dev
RUN apt-get clean

RUN mkdir -p /app
WORKDIR /app
ADD . /app
RUN yarn install

CMD ["node", "app.js"]
